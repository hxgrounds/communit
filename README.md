# CommUnit library

A collection of common code used in (m)unit tests.

-------------------------------------------------------------------------------

## Supported features

* **munit**
    - communit.munit.CommonTestMain
        - Allows to eliminate the TestMain in the project;
    - communit.munit.reporter.JUnitReportWriter
        - Fixes the junit reporter for nodejs; 

### Haxe targets support

* [x] js (node)
* [x] neko
* [x] python
* [x] java
* [x] cpp
* [x] csharp
    - Tested with mono
* [+-] lua
    - Asynchronous tests failing
* [ ] swf, as3, js
    - Tests failing (server not found)
* [ ] php
    - Untested
* [ ] haxelink
    - Untested


-------------------------------------------------------------------------------

## How to Use

##### CommonTestMain
- Set `-main communit.munit.CommonTestMain` in the test hxml file;
- Or extends CommonTestMain with a subclass

##### JunitReportWriter

Included automatically by CommonTestMain.

Optionally, include this snippet in your TestMain:

```haxe
    var runner:TestRunner = new TestRunner(client);
    ...
    var writer = new communit.munit.reporter.JUnitReportWriter(reportPath);
    writer.clearDirectory();
    runner.addResultClient(writer);
```

**Warning**: This reporter works only in `sys` targets or in nodejs.

-------------------------------------------------------------------------------

## Development

### Requirements

* [lix package manager](https://github.com/lix-pm/lix.client)
    - Install with npm: `npm install -g lix`

### Build and test

* Run: `lix install` to install requirements
* Run munit to test and build:
    - `lix run munit test -<language>`
    - Example:
        - `lix run munit test -js`
        - `lix run munit test -python`
        - ...

-------------------------------------------------------------------------------

## License

This work is licensed under the LGPL version 3 modified to restrict the use in
AI training datasets, based on these [non-ai-licenses][non-ai-licenses-repo] by
Aleksandar Prokopec.

In summary, this library may be used in any project, except for training AI models.
However internal changes should be released under the same license.

[non-ai-licenses-repo]: https://github.com/non-ai-licenses/non-ai-licenses